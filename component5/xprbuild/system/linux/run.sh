#! /bin/bash
## This script is used to run the project. It shuold contain the script which will run the project
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user
set -e

# Run the application

tomcat_dir=/usr/local/tomcat
cp target/yumpmsweb.war $tomcat_dir/webapps/
$tomcat_dir/bin/catalina.sh stop
$tomcat_dir/bin/catalina.sh start
