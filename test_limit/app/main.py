"""
This is a sample hello world app
It prints hello world 100 times
"""
__author__ = "Anushree"

import time


if __name__ == '__main__':

  ### $xpr_param_job_import
  start = 11
  end = 25

  for i in range(start, end):
    for j in range(2, i):
      if (i % j == 0):
        break
    else:
      print(i)

